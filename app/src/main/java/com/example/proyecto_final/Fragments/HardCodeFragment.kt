package com.example.proyecto_final.Fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.proyecto_final.Adapters.Pokemon
import com.example.proyecto_final.Adapters.PokemonAdapter
import com.example.proyecto_final.R
import kotlinx.android.synthetic.main.fragment_hard_code.*


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class HardCodeFragment : Fragment() {

    //Se crea el arreglo de objetos con los pokemons
    private val listaPokemonsHardcode= listOf(
        Pokemon("Pikachu","Electrico","Pikavoltio letal"),
        Pokemon("Charizard","Fuego Volador","Lanzallamas"),
        Pokemon("Squirtle","Agua","Hidrobomba"),
        Pokemon("Mewtwo","Psiquico","Supernova original"),
        Pokemon("Articuno","Hielo Volador","Rayo gélido"),
        Pokemon("Zapdos","Electrico Volador","Trueno"),
        Pokemon("Moltres","Fuego Volador","Fuego sagrado")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_hard_code, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvPokemons.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = PokemonAdapter(listaPokemonsHardcode)
        }
    }

    companion object {
        fun newInstance() : HardCodeFragment {
            return HardCodeFragment()
        }
    }


}