package com.example.proyecto_final.Fragments

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.proyecto_final.R
import kotlinx.android.synthetic.main.fragment_free.*
import kotlinx.android.synthetic.main.fragment_free.view.*


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class FreeFragment : Fragment() {

    companion object {
        fun newInstance() : FreeFragment {
            return FreeFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_free, container, false)

        rootView.sendButton.setOnClickListener {
            loadWebpage()
        }
        return rootView
    }

    @Throws(UnsupportedOperationException::class)
    fun buildUri(authority: String): Uri {
        val builder = Uri.Builder()
        builder.scheme("https")
            .authority(authority)
        return builder.build()
    }

    fun loadWebpage() {
        webview.loadUrl("")
        val uri: Uri
        try {
            uri = buildUri(uriText.text.toString())
            webview.loadUrl(uri.toString())
        } catch(e: UnsupportedOperationException) {
            e.printStackTrace()
        }
    }

}