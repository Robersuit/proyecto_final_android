package com.example.proyecto_final.Fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.proyecto_final.Adapters.Pokemon
import com.example.proyecto_final.Adapters.PokemonAdapter
import com.example.proyecto_final.R
import kotlinx.android.synthetic.main.fragment_hard_code.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class restFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        getColorFromJson(activity!!)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_rest, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    companion object {
        fun newInstance() : restFragment {
            return restFragment()
        }
    }

    private fun getColorFromJson(context: Context){
        doAsync {

            //Variable tipo RequestQueue -> Crea una peticion Volley
            val requestQueue = Volley.newRequestQueue(context)

            //Paso de parametros para crear el REQUEST
            var strRequest = object : StringRequest(Method.POST, "http://www.mocky.io/v2/5ceb387333000044547c397f", Response.Listener {
                    s-> Log.e("TAG", s.toString())

                val jsonResponse = JSONObject(s)
                var listaPokemons = listOf<Pokemon>()

                for (i in 0 until jsonResponse.getJSONArray("pokemons").length()) {
                    listaPokemons += Pokemon(
                        jsonResponse.getJSONArray("pokemons").getJSONObject(i).getString("name"),
                        jsonResponse.getJSONArray("pokemons").getJSONObject(i).getString("type"),
                        jsonResponse.getJSONArray("pokemons").getJSONObject(i).getString("attack")
                    )
                }

                rvPokemons.apply {
                    layoutManager = LinearLayoutManager(activity)
                    adapter = PokemonAdapter(listaPokemons)
                }

            }, Response.ErrorListener {
                e-> Log.e("TAG", e.toString())
            }){
            }

            requestQueue.add(strRequest)
        }
    }

}