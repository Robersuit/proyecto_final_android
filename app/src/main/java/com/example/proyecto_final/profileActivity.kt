package com.example.proyecto_final

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import java.io.File

class profileActivity : AppCompatActivity() {

    private lateinit var  photoView : ImageView
    private lateinit var  txtName : EditText
    private lateinit var  txtUser : EditText
    private lateinit var  txtPassword : EditText
    private lateinit var  checkRemember : CheckBox
    private lateinit var  btnUpdate : Button
    private lateinit var  btnClose : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        initComponents()
        setListeners()

    }

    private fun initComponents(){
        photoView = findViewById(R.id.photoView)
        txtName = findViewById(R.id.txtName)
        txtUser = findViewById(R.id.txtUser)
        txtPassword = findViewById(R.id.txtPassword)
        checkRemember = findViewById(R.id.checkRemember)
        btnUpdate = findViewById(R.id.btnUpdate)
        btnClose = findViewById(R.id.btnClose)

        var prefs: SharedPreferences = getSharedPreferences( "TablaMTI", Context.MODE_PRIVATE)
        txtName.setText( prefs.getString("nombre", "No existe"))
        txtUser.setText( prefs.getString("usuario", "No existe"))
        txtPassword.setText( prefs.getString("password", "No existe"))
        checkRemember.isChecked = prefs.getBoolean("remember", false)

        var bitmap: Bitmap = BitmapFactory.decodeFile( prefs.getString("photo", "No existe"))
        photoView.setImageBitmap(bitmap)

    }

    private fun setListeners(){

        btnUpdate.setOnClickListener {
            if (txtName.getText().toString() != "" && txtUser.text.toString() != "" && txtPassword.text.toString() != ""){
                //Mensaje PopUp

                var editor : SharedPreferences.Editor = getSharedPreferences( "TablaMTI", Context.MODE_PRIVATE).edit()
                editor.putString("nombre", txtName.text.toString())
                editor.putString("usuario", txtUser.text.toString())
                editor.putString("password", txtPassword.text.toString())
                editor.putBoolean("remember", checkRemember.isChecked)

                editor.commit()
                Toast.makeText(this,"Usuario actualizado con exito", Toast.LENGTH_SHORT).show()

            }else{
                Toast.makeText(this,"Favor de ingresar la información, todos los campos son obligatorios", Toast.LENGTH_SHORT).show()
            }
        }

        btnClose.setOnClickListener{
            this.finish()
        }
    }
}
