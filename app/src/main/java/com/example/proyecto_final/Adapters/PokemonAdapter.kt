package com.example.proyecto_final.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.example.proyecto_final.R
import kotlinx.android.synthetic.main.card_view.view.*

class PokemonAdapter(private val listaPokemons: List<Pokemon>) : RecyclerView.Adapter<PokemonAdapter.AdapterPokemon>() {

    // Este metodo regresa la vista donde se carga la info (cardview)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterPokemon {
        val inflater = LayoutInflater.from(parent.context)
        return AdapterPokemon(inflater, parent)
    }

    //Este metodo regresa el numero total de cardviews que se mostraran en RecyclerView
    override fun getItemCount(): Int {
        return listaPokemons.size
    }

    //Combina el objeto (Pokemon) con la vista (cardView)
    override fun onBindViewHolder(holder: AdapterPokemon, position: Int) {
        val pokemon: Pokemon = listaPokemons[position]
        holder.bind(pokemon)
    }

    //Se crea la clase que definira el comportamiento de la vista (cardView)
    class AdapterPokemon(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.card_view, parent, false)) {

        private var txtNamePokemon: TextView? = null
        private var txtTypePokemon: TextView? = null
        private var txtAttackPokemon: TextView? = null


        init {
            txtNamePokemon = itemView.findViewById(R.id.txtNamePokemon)
            txtTypePokemon = itemView.findViewById(R.id.txtTypePokemon)
            txtAttackPokemon = itemView.findViewById(R.id.txtAttackPokemon)
        }

        fun bind(pokemon: Pokemon) {
            txtNamePokemon?.text = "Nombre: "+pokemon.name
            txtTypePokemon?.text = "Tipo: "+pokemon.type
            txtAttackPokemon?.text = "Ataque: "+pokemon.attack
        }

    }
}