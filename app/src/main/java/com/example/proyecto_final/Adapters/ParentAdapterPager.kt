package com.example.proyecto_final.Adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.proyecto_final.Fragments.FreeFragment
import com.example.proyecto_final.Fragments.HardCodeFragment
import com.example.proyecto_final.Fragments.restFragment

class ParentAdapterPager (manager: FragmentManager) : FragmentPagerAdapter(manager){

    override fun getItem(position: Int): Fragment {
        when(position){
            0 ->  return HardCodeFragment.newInstance()
            1 ->  return restFragment.newInstance()
            else -> return FreeFragment.newInstance()
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Hardcode"
            1 -> "Rest"
            else -> "Free"
        }
    }

}