package com.example.proyecto_final

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.proyecto_final.Adapters.ParentAdapterPager

class homeActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {

        lateinit var tabs: TabLayout
        lateinit var viewPager: ViewPager
        lateinit var fb : FloatingActionButton

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        tabs = findViewById(R.id.tabs)
        viewPager = findViewById(R.id.viewpager)
        fb = findViewById(R.id.fbEjemplo)

        val adapter = ParentAdapterPager(supportFragmentManager)
        viewPager.adapter = adapter
        tabs.setupWithViewPager(viewPager)

        fb.setOnClickListener {
            Toast.makeText(this, "Soy un FloatingButton", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here.
        val id = item.getItemId()

        if (id == R.id.action_profile) {
            Toast.makeText(this, "Perfil", Toast.LENGTH_LONG).show()
            val intent = Intent(this, profileActivity::class.java)
            startActivity(intent)
            return true
        }
        if (id == R.id.action_logout) {

            var editor : SharedPreferences.Editor = getSharedPreferences( "TablaMTI", Context.MODE_PRIVATE).edit()
            editor.putBoolean("remember", false)
            editor.apply()

            Toast.makeText(this, "Salir", Toast.LENGTH_LONG).show()
            this.finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            return true
        }


        return super.onOptionsItemSelected(item)

    }


    /*private lateinit var  txtNombre : TextView
    private lateinit var  txtUsuario : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        initComponents()

        var prefs: SharedPreferences = getSharedPreferences( "TablaMTI", Context.MODE_PRIVATE)
        var nombre:String? = prefs.getString("nombre", "No existe")
        var usuario:String? = prefs.getString("usuario", "No existe")

        txtUsuario.text = usuario
        txtNombre.text = nombre

    }

    private fun initComponents(){
        txtNombre = findViewById(R.id.txtNombre)
        txtUsuario = findViewById(R.id.txtUsuario)
    }*/
}
