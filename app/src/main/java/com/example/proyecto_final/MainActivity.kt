package com.example.proyecto_final

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_register.*

class MainActivity : AppCompatActivity() {

    private lateinit var  txtUser : EditText
    private lateinit var  txtPassword : EditText
    private lateinit var  checkRemember : CheckBox
    private lateinit var  btnLogin : Button
    private lateinit var  btnNewUser : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initComponents()
        setListeners()

    }

    private fun initComponents(){
        txtUser = findViewById(R.id.txtUser)
        txtPassword = findViewById(R.id.txtPassword)
        checkRemember = findViewById(R.id.checkRemember)
        btnLogin = findViewById(R.id.btnLogin)
        btnNewUser = findViewById(R.id.btnNewUser)
    }

    private fun setListeners(){
        btnLogin.setOnClickListener {

            if (loginUsers(txtUser.text.toString(), txtPassword.text.toString())){
                if(checkRemember.isChecked) setRemember()

                //Cambio de activity y mensaje de bienvenida
                Toast.makeText(this, "Bienvenido "+txtUser.text.toString(), Toast.LENGTH_SHORT).show()
                val intent = Intent(this, homeActivity::class.java)
                startActivity(intent)

            }else Toast.makeText(this,"Usuario y/o contraseña incorrectos", Toast.LENGTH_SHORT).show()
        }

        btnNewUser.setOnClickListener{
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setRemember(){
        var editor : SharedPreferences.Editor = getSharedPreferences( "TablaMTI", Context.MODE_PRIVATE).edit()
        editor.putBoolean("remember", checkRemember.isChecked)
        editor.apply()
    }

    private fun loginUsers(user: String, password: String): Boolean{

        var prefs: SharedPreferences = getSharedPreferences( "TablaMTI", Context.MODE_PRIVATE)
        var usuario:String? = prefs.getString("usuario", "No existe")
        var password:String? = prefs.getString("password", "No existe")

        return user == usuario && password == password
    }
}
